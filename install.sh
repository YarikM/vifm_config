#! /bin/bash 

[ -n "$DISTRO" ] || exit 1

git clone https://github.com/vifm/vifm.git || exit 1;

VIFMCFG='vifmcfg'

[[ "$DISTRO" =~ "archlinux" ]] && VIFM_HOME_DIR="${HOME}/.vifm"
[[ "$DISTRO" =~ "ubuntu" ]] && VIFM_HOME_DIR="${HOME}/.config/vifm" && ln -sf $VIFM_HOME_DIR ~/.vifm
[[ "$DISTRO" =~ "debian" ]] && VIFM_HOME_DIR="${HOME}/.config/vifm" && ln -sf $VIFM_HOME_DIR ~/.vifm

[ -d "$VIFM_HOME_DIR" ] && rm -frv "$VIFM_HOME_DIR"

pushd ${PWD}
  cd vifm 
  autoreconf -f -i && \
  ./configure && \
  sudo make install
popd
rm -frv vifm

mkdir -p "${VIFM_HOME_DIR}"
ln -sf ${PWD}/${VIFMCFG}/colors     ${VIFM_HOME_DIR}/colors
ln -sf ${PWD}/${VIFMCFG}/vifmrc     ${VIFM_HOME_DIR}/vifmrc

for item in ${PWD}/${VIFMCFG}/*.vifm; do 
  ln -sf ${item} ${VIFM_HOME_DIR}/$(basename $item) 
done
